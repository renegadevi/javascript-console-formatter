# Javascript console formatter

Simple formatter that adds timestamps, symbols and printed in a more structural formatting.

- No dependencies.
- Works with and without node.js

![Screenshot](https://gitlab.com/renegadevi/javascript-console-formatter/-/raw/main/screenshot.png)
![Screenshot2](https://gitlab.com/renegadevi/javascript-console-formatter/-/raw/main/screenshot2.png)


## Getting started

### NPM, Yarn, Pnpm
https://www.npmjs.com/package/js-console-formatter

```sh
npm i js-console-formatter
```

```sh
yarn add js-console-formatter
```

```sh
pnpm add js-console-formatter
```

Then import it:
```js
import jsConsoleFormatter from 'js-console-formatter'
```


### Vanilla JS
1. Download the min.js file
2. import it with your relative path
```js
# Example
import "./assets/js/console_formatter.min.js";
```


## How to use
Beside replacing your normal console outputs, it also adds a console.success function.
```js
console.success("A message from console.success()");
console.log("A message from console.log()");
console.debug("A message from console.debug()");
console.info("A message from console.info()");
console.error("A message from console.error()");
console.warn("A message from console.warn()");
```

## Tested and works on:
- Windows 10: Chrome and Edge
- MacOS Monterey: Chrome

**Notes**
Safari is not supported. If Safari is detected, it will be ignored.

## FAQ
**Why cant't I see any logs in Chrome?**

This is problably due to the logging levels set in the dev-tools console. On the right side, there's a "levels" dropdown. Select 'Verbose'

## License
MIT
