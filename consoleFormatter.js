/*
  JS Console formatter v.1.1.1

  Simple formatter that adds timestamps, symbols and printed in a more
  structural formatting.

  Copyright (c) 2022 Philip Andersen

  Commands:
    console.success() ->  ✅
    console.log()     ->  ❗️
    console.debug()   ->  🔨
    console.info()    ->  📝
    console.error()   ->  ⛔️
    console.warn()    ->  🚧

  Supported browsers:
    Chrome, Edge

  Unsupported browsers:
    Safari (will be ignored)
*/

/**
 * Settings for stack tracing
 *
 *  STACK_TRACE
 *    - enable/disable stack tracing
 *
 *  STACK_TRACE_SKIP
 *    - Skip initial tracing loops,
 *      0: trace back to this file (consoleFormatter())
 *      1: start at call point
 */
 const STACK_TRACE = true;
 const STACK_TRACE_SKIP = 1;

 /**
  * Get current timestamp
  *
  * Example:
  *   [05:47:39.610] GMT+0200 (centraleuropeisk sommartid)
  */
 function getTimestamp() {
   // Get current date
   const date = new Date();

   // Get time
   let hour = date.getHours();
   let minutes = date.getMinutes();
   let seconds = date.getSeconds();
   let milliseconds = date.getMilliseconds();

   // HH:MM:SS.ms
   let time =
     (hour < 10 ? "0" + hour : hour) +
     ":" +
     (minutes < 10 ? "0" + minutes : minutes) +
     ":" +
     (seconds < 10 ? "0" + seconds : seconds) +
     "." +
     ("00" + milliseconds).slice(-3);

   // GMT+0200
   let timezone = date.toTimeString().split(" ")[1];

   // (central european time)
   let timezone_name = " (" + date.toTimeString().split(" (")[1];

   // [05:47:39.610] GMT+0200 (centraleuropeisk sommartid)
   return `[${time}] ${timezone}${timezone_name}`;
 }

 /**
  * Get a stack trace from console
  *
  * If there is a error stack, format it, else return console.trace();
  */
 function getStackTrace() {
   // get stack
   let stack;
   if ((stack = new Error().stack)) {
     // initialize string to return
     let formattedTrace = "\n";

     // format stack trace
     stack
       .split("at ")
       .slice(STACK_TRACE_SKIP)
       .forEach((file, index) => {
         if (index > 1) {
           formattedTrace += `\n\t\t\t\t🧩 ${index - 1}: ${file.split(" (")[0]}`;
           if (file.split(" (")[1]) {
             formattedTrace += `\n\t\t\t\t${
               file.split(" (")[1].split(")")[0]
             }\n`;
           }
         }
       });

     // return formatted stack trace
     return formattedTrace;
   } else {
     // return unformatted.
     return console.trace();
   }
 }

 // Define formatter
 var consoleFormatter = (function () {
   // console.x functions
   return {
     log: function () {
       console._debug.call(
         console,
         "❗️\tLOG\t\t|",
         getTimestamp(),
         "\n\t\t\t|",
         ...arguments
       );
     },
     info: function () {
       console._debug.call(
         console,
         "📝\tINFO\t|",
         getTimestamp(),
         "\n\t\t\t|",
         ...arguments
       );
     },
     warn: function () {
       console._debug.call(
         console,
         "🚧\tWARN\t|",
         getTimestamp(),
         "\n\t\t\t|",
         ...arguments,
         STACK_TRACE ? getStackTrace() : ""
       );
     },
     debug: function () {
       console._debug.call(
         console,
         "🔨\tDEBUG\t|",
         getTimestamp(),
         "\n\t\t\t|",
         ...arguments
       );
     },
     success: function () {
       console._debug.call(
         console,
         "✅\tSUCCESS\t|",
         getTimestamp(),
         "\n\t\t\t|",
         ...arguments
       );
     },
     error: function () {
       console._debug.call(
         console,
         "⛔️\tERROR\t|",
         getTimestamp(),
         "\n\t\t\t|",
         ...arguments,
         STACK_TRACE ? getStackTrace() : ""
       );
     }
   };
 })(window.console);

 // Enable formatter for global console messages
 // NOTE: Safari is not supported.
 var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
 if (!isSafari) {
   // Define console
   window.console = console;

   console.log = consoleFormatter.log;
   console.info = consoleFormatter.info;
   console.warn = consoleFormatter.warn;
   console.success = consoleFormatter.success;
   console.error = consoleFormatter.error;

   // This is the magic trick to make debug work.
   console._debug = console.debug;
   console.debug = consoleFormatter.debug;
 }
